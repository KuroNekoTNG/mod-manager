﻿using System;

namespace ModManagerCore.CoreAttributes {
	/// <summary>
	/// Used to have static constructors called to set them up for the plugin's use.
	/// </summary>
	public class StaticConstructorOnLoad : Attribute {
	}
}
