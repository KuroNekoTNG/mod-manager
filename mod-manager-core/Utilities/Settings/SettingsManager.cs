﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

using ModManagerCore.Exceptions;
using ModManagerCore.Utilities.Logging;

using Newtonsoft.Json;

namespace ModManagerCore.Utilities.Settings {
	/// <summary>
	/// Handles the settings of the program.
	/// </summary>
	public sealed class SettingsManager {

		#region Static

		/// <summary>
		/// The absolute path to where the setting files are stored.
		/// </summary>
		public static readonly string SETTING_LOCATION;

		/// <summary>
		/// How the json files are stored as.
		/// </summary>
		public static readonly JsonSerializerSettings JSON_SETTINGS;

		/// <summary>
		/// Used to retrive the SettingsManager singleton.
		/// </summary>
		public static SettingsManager Singleton {
			get;
		}

		static SettingsManager() {
			// Checks to see if the code is running on Windows, and creating an absolute path to the configuration directory.
			if ( RuntimeInformation.IsOSPlatform( OSPlatform.Windows ) ) {
				SETTING_LOCATION = Path.Combine( // Set's the SETTING_LOCATION readonly variable.
						Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ), // Grabs the path to the local app data.
						"Mod Manager", // Includes the Mod Manager directory.
						"Config" ); // Includes the Config directory.
			} else if ( RuntimeInformation.IsOSPlatform( OSPlatform.Linux ) ) { // Checks to see if the code is running on Linux.
				SETTING_LOCATION = Path.Combine( // Set's the SETTING_LOCATION readonly variable.
						Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData ), // Get's the path to the AppData folder.
						"Mod Manager" ); // Includes the Mod Manager folder.
			} else if ( RuntimeInformation.IsOSPlatform( OSPlatform.OSX ) ) { // Checks to see if the code is running on macOS X.
				SETTING_LOCATION = Path.Combine( // Set's the SETTING_LOCATION readonly variable.
					Environment.GetFolderPath( Environment.SpecialFolder.UserProfile ), // Grabs the path to the user home dir.
					"Library", // Includes the Library folder.
					"Application Support", // Includes the Application Support folder.
					"Mod Manager" ); // Includes the Mod Manager folder.
			} else {
				SETTING_LOCATION = Path.Combine( // Set's the SETTING_LOCATION readonly variable.
					Path.GetDirectoryName( Assembly.GetExecutingAssembly().CodeBase ), // Get's the path of the assembly.
					"Config" ); // Includes the Config folder.
			}

			if ( !Directory.Exists( SETTING_LOCATION ) ) { // Makes sure the setting directory exists
				Directory.CreateDirectory( SETTING_LOCATION ); // Creates the directory if doesn't exist
			}

			JSON_SETTINGS = new JsonSerializerSettings() { // This is created here since it cleans up the scope outside methods.
				ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor, // This is to allow Newtonsoft.Json to do what the name suggests.
				DateFormatHandling = DateFormatHandling.IsoDateFormat, // ISO Date format is better than Microsoft's.
				DateParseHandling = DateParseHandling.DateTimeOffset, // Uses the DateTimeOffset as it uses UTC.
				DateTimeZoneHandling = DateTimeZoneHandling.Utc, // I don't understand why anyone would not want to use it.
				FloatFormatHandling = FloatFormatHandling.String, // Converts floats to strings.
				FloatParseHandling = FloatParseHandling.Double, // When parsing them back to floats, use double. Precision is not needed.
				NullValueHandling = NullValueHandling.Include, // This is so that all values are represented in the json.
				ObjectCreationHandling = ObjectCreationHandling.Auto, // Newtonsoft.Json will decide how to create an object.
				PreserveReferencesHandling = PreserveReferencesHandling.Arrays, // Preserves refrences to arrays.
				ReferenceLoopHandling = ReferenceLoopHandling.Ignore, // So Newtonsoft doesn't throw an exception nor creates huge json files.
				TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple, // This is so specific assemblies are not needed when deserializing.
				TypeNameHandling = TypeNameHandling.All // So objects are properly deserialized.
			};

			Singleton = new SettingsManager(); // Creates a new version of the singleton.
		}

		#endregion

		/// <summary>
		/// Indexer to either retrieve a value or set a value.
		/// It is recommended not to use this unless certian that the value exists.
		/// <seealso cref="GetSetting(string, string)"/>
		/// <seealso cref="SetSetting(string, string, object)"/>
		/// </summary>
		/// <param name="plugin">The plugin that is calling.</param>
		/// <param name="key">The key to access the value.</param>
		/// <returns>The value if found, otherwise it will throw an exception.</returns>
		public object this[string plugin, string key] {
			get => GetSetting( plugin, key );
			set => SetSetting( plugin, key, value );
		}

		/// <summary>
		/// Used to get an array of plugin directories.
		/// </summary>
		public string[] Plugins {
			get {
				List<string> plugins = new List<string>();

				foreach ( string file in Directory.EnumerateFiles( SettingsManager.SETTING_LOCATION ) ) {
					string fileName = Path.GetFileNameWithoutExtension( file );

					plugins.Add( fileName );
				}

				return plugins.ToArray();
			}
		}

		private LogTalker log;

		/// <summary>
		/// Private constructor of the singleton.
		/// </summary>
		private SettingsManager() {
			log = new LogTalker( this );
		}

		/// <summary>
		/// Used to retrieve keys from setting files.
		/// </summary>
		/// <param name="plugin">The name of the plugin.</param>
		/// <returns>An array of string key's that are used.</returns>
		public string[] GetKeysFromSetting( string plugin ) {
			Dictionary<string, object> setting = OpenPluginSetting( plugin );
			List<string> keys = new List<string>( setting.Keys );

			return keys.ToArray();
		}

		/// <summary>
		/// Used to retrieve a value from setting.
		/// Will throw an exception if it cannot find the key.
		/// <seealso cref="TryGetSetting(string, string, out object)"/>
		/// <seealso cref="this[string, string]"/>
		/// </summary>
		/// <param name="plugin">The plugin that is calling.</param>
		/// <param name="key">The key to access the value.</param>
		/// <returns>The value if found, otherwise it will throw an exception.</returns>
		public object GetSetting( string plugin, string key ) {
			Dictionary<string, object> setting = OpenPluginSetting( plugin ); // This method opens the setting file for reading.

			return setting[key]; // Returns the value associated with that key.
		}

		/// <summary>
		/// Attempts to retrieve a value from settings.
		/// </summary>
		/// <param name="plugin">The plugin that is calling.</param>
		/// <param name="key">The key to get to the value.</param>
		/// <param name="value">If any value is found, is placed here, otherwise is null.</param>
		/// <returns>True if a value was found with the key, otherwise false.</returns>
		public bool TryGetSetting( string plugin, string key, out object value ) {
			log.Message( $"Attempting to retrieve a value from '{plugin}.json'." );

			try {
				value = GetSetting( plugin, key ); // Calls a method that would do mostly the same thing.
			} catch ( Exception e ) { // Type of exception does not matter.
				value = null; // Cannot out a value unless the method has one.

				log.Message( e, MessageType.Error ); // Write the exception to the log.

				return false; // Notify failure to get the value.
			}

			return true; // Noity success of getting the value.
		}

		/// <summary>
		/// Set's a value to a key within the settings.
		/// </summary>
		/// <param name="plugin">The name of the plugin.</param>
		/// <param name="key">The key to represent the value.</param>
		/// <param name="value">The object to store in settings.</param>
		public void SetSetting( string plugin, string key, object value ) {
			Dictionary<string, object> pluginSettings = OpenPluginSetting( plugin );

			if ( pluginSettings is null ) { // Since OpenPluginSetting will return null upon failure, it checks to see if it is null.
				throw new SettingNotFoundException( plugin ); // Uses a custom exception so it is more detailed off the bat.
			}

			pluginSettings[key] = value; // This method does not do any checks, so that is why it uses the indexer.

			SavePluginSetting( plugin, pluginSettings ); // Attempts to saves the changes to the file.
		}

		/// <summary>
		/// Attempts to set a value to the setting.
		/// <seealso cref="SetSetting(string, string, object)"/>
		/// </summary>
		/// <param name="plugin">The name of the plugin.</param>
		/// <param name="key">The key to access the value.</param>
		/// <param name="value">The value to save with the key.</param>
		/// <returns><see langword="true"/> if successful, otherwise <see langword="false"/>.</returns>
		public bool TrySetSetting( string plugin, string key, object value ) {
			try {
				SetSetting( plugin, key, value ); // Don't need to do what was already done.
			} catch {
				return false; // Since an exception was thrown, just return false.
			}

			return true; // No exception was thrown, so return true.
		}

		/// <summary>
		/// Attempts to create a setting file for a plugin.
		/// This should be called first to ensure that the plugin has regestered.
		/// </summary>
		/// <param name="plugin">The name of the plugin.</param>
		public void CreateSetting( string plugin ) {
			string path = Path.Combine( SETTING_LOCATION, string.Format( "{0}.json", plugin ) ); // Just recreates the path that the json should be at and named.

			if ( File.Exists( path ) ) { // Just so it fits with what the method does.
				SavePluginSetting( plugin, new Dictionary<string, object>() ); // Method already creates a new file, just pass it an empty dictionary. 
			}
		}

		/// <summary>
		/// Attempts to create the settings used by the plugin passed to it.
		/// <seealso cref="CreateSetting(string)"/>
		/// </summary>
		/// <param name="plugin">The name of the plugin.</param>
		/// <returns><see langword="true"/> if everything went smoothly, otherwise <see langword="false"/>.</returns>
		public bool TryCreateSetting( string plugin ) {
			try {
				CreateSetting( plugin ); // Just try-catch the other method.
			} catch {
				return false; // An exception was thrown, so represent false.
			}

			return true; // It was successful, so return true.
		}

		/// <summary>
		/// Checks to see if a key exists within the settings.
		/// </summary>
		/// <param name="plugin">The name of the plugin.</param>
		/// <param name="key">The key to check for.</param>
		/// <returns><see langword="true"/> if the key does exist in setting, otherwise <see langword="false"/>.</returns>
		public bool KeyExists( string plugin, string key ) {
			Dictionary<string, object> setting = OpenPluginSetting( plugin );

			return setting.ContainsKey( key );
		}

		/// <summary>
		/// Checks to see if a key exists within the settings.
		/// </summary>
		/// <param name="plugin">The name of the plugin.</param>
		/// <param name="obj">The value to check for.</param>
		/// <returns><see langword="true"/> if the value does exist in setting, otherwise <see langword="false"/>.</returns>
		public bool ValueExists( string plugin, object obj ) {
			Dictionary<string, object> setting = OpenPluginSetting( plugin );

			return setting.ContainsValue( obj );
		}

		// This method is used to save anything that might be changed into a setting file.
		private void SavePluginSetting( string plugin, Dictionary<string, object> setting ) {
			string json = JsonConvert.SerializeObject( setting, JSON_SETTINGS ); // Grabs the JSON from Newtonsoft because Microsoft's JSON Serializer is grabage.

			try {
				using ( FileStream pluginFile = new FileStream( // To not forget to call Dispose() later.
					Path.Combine( SETTING_LOCATION, string.Format( "{0}.json", plugin ) ), // Quick and easy way to get the plugin file.
					FileMode.Create, // Will be overriding it if it exists or creating if one does not.
					FileAccess.Write, // Will only be writing to it.
					FileShare.Read ) ) { // Do not want other programs to write to it, reading is fine.
					using ( StreamWriter fileWriter = new StreamWriter( pluginFile ) ) { // To make it easier to write a string out to a file.
						fileWriter.Write( json ); // Writes the string out.
						fileWriter.Flush(); // Flushes the writing stream.
					}

					pluginFile.Flush(); // Flushes the file stream. Ensures what has been written is commited.
				}
			} catch ( Exception e ) { // Does not matter what type of exception.
				log.Message( e, MessageType.Exception ); // Writes it out to a log.
			}
		}

		// Opens a plugin setting file for reading by other methods.
		// Reason it exists is for no rewriting of code.
		private Dictionary<string, object> OpenPluginSetting( string plugin ) {
			Dictionary<string, object> pluginSetting;
			string json;

			try {
				using ( FileStream pluginFile = new FileStream( // Allows for me to not call Dispose.
					Path.Combine( SETTING_LOCATION, string.Format( "{0}.json", plugin ) ), // Generate the path to the plugin's json.
					FileMode.Open, // I don't want to create the file.
					FileAccess.Read, // I am not writing anything to the file.
					FileShare.Read ) ) { // I don't want anything else writing to the file.
					using ( StreamReader fileReader = new StreamReader( pluginFile ) ) { // Too lazy to do all the things to convert a byte[] to a string.
						json = fileReader.ReadToEnd();
					}
				}

				pluginSetting = JsonConvert.DeserializeObject<Dictionary<string, object>>( json, JSON_SETTINGS ); // Allows Newtonsoft.Json to know which type to export to.
			} catch ( Exception e ) { // Doesn't matter the type of exception.
				log.Message( e, MessageType.Exception ); // Writes the exception out.

				pluginSetting = null; // No need for 2 return statements.
			}

			return pluginSetting; // Returns the pointer to the object.
		}
	}
}