﻿using System;

namespace ModManagerCore.Utilities.Settings {
	/// <summary>
	/// Used to represent the plugin's setttings.
	/// </summary>
	public sealed class SettingsRep {

		/// <summary>
		/// The name of the plugin.
		/// </summary>
		public string PluginRepName {
			get;
		}

		private SettingsManager settings;

		/// <summary>
		/// The default constructor of the class.
		/// </summary>
		/// <param name="plugin">Takes in the name of the plugin.</param>
		public SettingsRep( string plugin ) {
			PluginRepName = plugin;

			settings = SettingsManager.Singleton;

			settings.TryCreateSetting( plugin );
		}

		/// <summary>
		/// Converts the type of the plugin to the name of the plugin.
		/// </summary>
		/// <param name="type">The type of the plugin.</param>
		public SettingsRep( Type type ) : this( type.Name ) {

		}

		/// <summary>
		/// Retrieves a value from settings using the known plugin name.
		/// </summary>
		/// <param name="key">The key to access the value.</param>
		/// <param name="value">The value associated with the key.</param>
		/// <returns><see langword="true"/> if it was able to get the value.</returns>
		public bool GetSettingValue( string key, out object value ) => settings.TryGetSetting( PluginRepName, key, out value );

		/// <summary>
		/// Set's the value in settings using the known plugin name.
		/// </summary>
		/// <param name="key">The key to change its associated value.</param>
		/// <param name="value">The value to change it to.</param>
		/// <returns><see langword="true"/> if it was successful.</returns>
		public bool SetSettingValue( string key, object value ) => settings.TrySetSetting( PluginRepName, key, value );

		/// <summary>
		/// Used to check if a value already exists in settings.
		/// </summary>
		/// <param name="value">The value to see if it exists.</param>
		/// <returns><see langword="true"/> if the value exists in settings.</returns>
		public bool ValueExists( object value ) => settings.ValueExists( PluginRepName, value );

		/// <summary>
		/// Used to check if a key already exists in settings.
		/// </summary>
		/// <param name="key">The key to check for.</param>
		/// <returns><see langword="true"/> if a key is found that matches the one passed.</returns>
		public bool KeyExists( string key ) => settings.KeyExists( PluginRepName, key );
	}
}