﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

using ModManagerCore.Utilities.Logging;

namespace ModManagerCore.Utilities {
	/// <summary>
	/// This static class is meant to make it easier to extract data from an <see cref="Assembly"/>.
	/// </summary>
	public static class EmbededResourceLoader {

		private static LogTalker log; // Holds the log talker.

		// Static constructor.
		static EmbededResourceLoader() {
			log = new LogTalker( typeof( EmbededResourceLoader ) ); // Creates a new LogTalker with the type of this class.
		}

		/// <summary>
		/// Loads the text from the <see cref="Assembly"/> when it is embeded.
		/// <seealso cref="LoadTextFromAssembly(Assembly, string)"/>
		/// </summary>
		/// <param name="type">The <see cref="Type"/> to get to the <see cref="Assembly"/>.</param>
		/// <param name="id">The ID of the text.</param>
		/// <returns>A <see cref="string"/> that was embeded into the <see cref="Assembly"/>.</returns>
		public static string LoadTextFromAssembly( Type type, string id ) => LoadTextFromAssembly( type.Assembly, id );

		/// <summary>
		/// Loads the text from the <see cref="Assembly"/> when it is embeded.
		/// <seealso cref="LoadTextFromAssembly(Type, string)"/>
		/// </summary>
		/// <param name="asm">The <see cref="Assembly"/> where the text is embeded into.</param>
		/// <param name="id">The ID of the string to pull out of.</param>
		/// <returns>A string that was embeded.</returns>
		public static string LoadTextFromAssembly( Assembly asm, string id ) {
			byte[] data = LoadRawDataFromAssembly( asm, id ); // Grabs the raw data of the string.
			string str;

			log.Message( "Converting raw data into a string." );

			str = Encoding.Default.GetString( data ); // Converts the bytes to the default type.
			str = str.Substring( 3 ); // Cuts off the first 3 random chars.

			return str; // Returns the string.
		}

		/// <summary>
		/// Loads raw data from the <see cref="Assembly"/>.
		/// <seealso cref="LoadRawDataFromAssembly(Assembly, string)"/>
		/// </summary>
		/// <param name="type">The <see cref="Type"/> to get to the <see cref="Assembly"/>.</param>
		/// <param name="id">The ID of the data embeded into the <see cref="Assembly"/>.</param>
		/// <returns>A <see cref="byte"/> array of raw data.</returns>
		public static byte[] LoadRawDataFromAssembly( Type type, string id ) => LoadRawDataFromAssembly( type.Assembly, id );

		/// <summary>
		/// Load raw data from the <see cref="Assembly"/>.
		/// <seealso cref="LoadRawDataFromAssembly(Type, string)"/>
		/// </summary>
		/// <param name="asm">THe <see cref="Assembly"/> to get the embeded data.</param>
		/// <param name="id">The ID of the data that is embeded into the <see cref="Assembly"/>.</param>
		/// <returns>A <see cref="byte"/> array of data that is embeded in the assembly.</returns>
		public static byte[] LoadRawDataFromAssembly( Assembly asm, string id ) {
			byte[] rawData;

			log.Message( "Extracting data from an assembly." );

			using ( Stream asmStream = asm.GetManifestResourceStream( id ) ) { // Used to auto-dispose of the stream.
				rawData = new byte[asmStream.Length]; // Creates a new byte[] with the length of the stream.

				asmStream.Read( rawData, 0, rawData.Length ); // Reads the bytes into the array.
			}

			return rawData; // Returns the byte array.
		}
	}
}