﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace ModManagerCore.Utilities.Logging {
	/// <summary>
	/// LogManager is meant to manage log files outputting and inputting.
	/// It also holds data as to where the files are at.
	/// You cannot directly write to the log files and instead must go through <see cref="LogTalker"/>.
	/// </summary>
	public class LogManager {

		#region Static

		/// <summary>
		/// The location of the log files.
		/// </summary>
		public static string LOG_FILE_LOCATION {
			get;
		}

		/// <summary>
		/// The abosulte path to the verbose log file.
		/// </summary>
		public static string VerboseFile {
			get;
		}

		/// <summary>
		/// The absolute path to the error log file.
		/// </summary>
		public static string ErrorFile {
			get;
		}

		/// <summary>
		/// The absolute path to the exception log file.
		/// </summary>
		public static string ExceptionFile {
			get;
		}

		/// <summary>
		/// Used to retrive the LogManager singleton.
		/// </summary>
		public static LogManager Singleton {
			get;
		}

		static LogManager() {
			// This if tree exists because storing data is different per operating system. And not using Environment.OSPlatform as that only allows for Unix and Win32NT.
			if ( RuntimeInformation.IsOSPlatform( OSPlatform.Windows ) ) {
				LOG_FILE_LOCATION = Path.Combine( // Starts combining the path.
					Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ), // Get's the local app data as that is not meant for roaming.
					"Mod Manager", // The name of the program.
					"Log" ); // The name of the folder.
			} else if ( RuntimeInformation.IsOSPlatform( OSPlatform.OSX ) ) {
				LOG_FILE_LOCATION = Path.Combine( // Starts combining the path.
					Environment.GetFolderPath( Environment.SpecialFolder.UserProfile ), // Get's the user home directory.
					"Library", // Appends the library folder.
					"Logs", // Appends the Application Support folder.
					"Mod Manager" ); // Appends the name of the program.
			} else if ( RuntimeInformation.IsOSPlatform( OSPlatform.Linux ) ) {
				LOG_FILE_LOCATION = Path.Combine( // Starts combining the path.
					Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData ), // Get's the .config folder of Linux.
					"Mod Manager", // Appends the name of the program.
					"Log" ); // Appends the folder name.
			} else {
				LOG_FILE_LOCATION = "Log"; // Set's it to the local dir.
			}

			// Creates the path to the files since the path to the folder is alread known.
			VerboseFile = Path.Combine( LOG_FILE_LOCATION, "Verbose.log" );
			ErrorFile = Path.Combine( LOG_FILE_LOCATION, "Error.log" );
			ExceptionFile = Path.Combine( LOG_FILE_LOCATION, "Exception.log" );

			// Checks if the directory exists, and if it doesn't to make one.
			if ( !Directory.Exists( LOG_FILE_LOCATION ) ) {
				Directory.CreateDirectory( LOG_FILE_LOCATION );
			}

			Singleton = new LogManager(); // Creates a new version of the singleton.
		}

		#endregion

		/// <summary>
		/// Used to tell the LogManager to write out verbose messages or not.
		/// </summary>
		public bool WriteVerbose {
			get;
			set;
		}

		/// <summary>
		/// A way to notify writing of a log.
		/// </summary>
		/// <param name="log">The log that is being written out.</param>
		public delegate void ProcessingLog( LogMessage log );

		/// <summary>
		/// This field is used to subscribe to get a method to be notified that a message is being written out.
		/// </summary>
		public ProcessingLog processingLog;

		private FileStream verboseLog, errorLog, exceptionLog; // NOTHING SHOULD BE USING THESE OTHER THAN THE LOG THREAD!
		private ManualResetEvent newLog, quitting;
		private ConcurrentQueue<LogMessage> queuedLogs;
		private Thread logThread;

		/// <summary>
		/// Private constructor of the singleton.
		/// </summary>
		private LogManager() {
			// Creates file streams to write to the log files.
			verboseLog = new FileStream( VerboseFile, FileMode.Create, FileAccess.Write, FileShare.Read, 4096, FileOptions.RandomAccess );
			errorLog = new FileStream( ErrorFile, FileMode.Create, FileAccess.Write, FileShare.Read, 4096, FileOptions.RandomAccess );
			exceptionLog = new FileStream( ExceptionFile, FileMode.Create, FileAccess.Write, FileShare.Read, 4096, FileOptions.RandomAccess );
			// Creates events to notify the thread of any new things.
			newLog = new ManualResetEvent( false );
			quitting = new ManualResetEvent( false );

			queuedLogs = new ConcurrentQueue<LogMessage>();

			logThread = new Thread( () => {
				int status;

				// Infinite loop so the thread does not die and writing to the thread does not stop.
				while ( true ) {
					status = WaitHandle.WaitAny( new WaitHandle[] { newLog, quitting } ); // So the thread knows what operation to do next.

					// So the thread can act on the action.
					// Status of 0 means that a new log is waiting to be written.
					// Status of 1 means the program is quitting.
					// Any other status is undefined behaviour.
					switch ( status ) {
						case 0:
							WriteToLog();
							break;
						case 1:
							FlushAndDispose();
							return;
						default:
							throw new Exception( string.Format( "Unknown status: '{0}'", status ) );
					}

					newLog.Reset(); // Since quitting terminates the loop, it is only reasonable to only reset newLog.
				}
			} ) {
				Name = "Logging Thread",
				Priority = ThreadPriority.Lowest
			};

			logThread.Start();

			AppDomain.CurrentDomain.ProcessExit += ( _, __ ) => {
				quitting.Set(); // Forwards the message to raise the flag for the thread to quit.
			};
		}

		/// <summary>
		/// Used to inform the logging thread that the program is closing.
		/// Use this if there is a UI placed on top of the core, as the core as no way of knowing if the program is closing.
		/// </summary>
		public void InitiateClosing() => quitting.Set();

		internal void EnqueueLog( string message, string from, Exception e = null, MessageType type = MessageType.Verbose ) => EnqueueLog( new LogMessage( message, from, e, type ) );

		internal void EnqueueLog( LogMessage log ) {
			queuedLogs.Enqueue( log ); // Enqueues the log to be written.

			newLog.Set(); // Raises the flag for the thread to know a log is waiting to be written.
		}

		private void FlushAndDispose() {
			// Will loop until the queue is empty.
			while ( !queuedLogs.IsEmpty ) {
				WriteToLog(); // Calls this to ensure queue is empty.
			}

			// Starts flushing and disposing of streams.
			verboseLog.Flush( true );
			verboseLog.Dispose();

			errorLog.Flush( true );
			verboseLog.Dispose();

			exceptionLog.Flush( true );
			exceptionLog.Dispose();
		}

		private void WriteToLog() {
			FileStream logFile;
			string msg;

			// Loops through the concurrent queue until all logs are removed to ensure they are all written to the file.
			while ( queuedLogs.TryDequeue( out LogMessage log ) ) {
				msg = log.StandardOutput; // Get's the standard output of the log.

				// Checks to see which log type it is so it knows where to write it.
				if ( log.Type == MessageType.Verbose && WriteVerbose ) {
					logFile = verboseLog; // Grabs the verbose log stream.
				} else if ( log.Type == MessageType.Error ) {
					logFile = errorLog; // Grabs the error log stream.
				} else {
					logFile = exceptionLog; // Grabs the exception log stream.
				}

				WriteToLog( logFile, msg ); // Writes it out to the log.

				processingLog?.Invoke( log ); // Attempts to invoke any methods that are part of the delegate.
			}
		}

		private void WriteToLog( FileStream log, string message ) {
			// Uses a StreamWriter to not bother with converting it to bytes.
			// It also uses the default encoding, 4096 of buffer, and leaves the stream open.
			// It leaves the stream open so as to not close when the writer is done.
			using ( StreamWriter logWriter = new StreamWriter( log, Encoding.Default, 4096, true ) ) {
				logWriter.AutoFlush = true; // Auto writes to the stream.

				logWriter.WriteLine( message ); // Writes the message and ends it with the new line char to ensure space for the next message.
			}

			log.Flush( true ); // Flushes to log stream.
		}
	}
}