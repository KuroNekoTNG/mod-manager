﻿using System;

namespace ModManagerCore.Utilities.Logging {

	/// <summary>
	/// LogTalker is a <see langword="sealed"/> class that represents the object in the log.
	/// </summary>
	public sealed class LogTalker {

		/// <summary>
		/// The name that will show up on the log files.
		/// </summary>
		public string RepName {
			get;
		}

		private LogManager manager;

		/// <summary>
		/// Takes in an object so it can get the name that it will represent in the logs.
		/// </summary>
		/// <param name="obj">The object to represent.</param>
		public LogTalker( in object obj ) : this( obj.GetType() ) {
			// This constructor is left empty as it calls the other constructor with the type of the object.
		}

		/// <summary>
		/// Takes in the type of object it is representing.
		/// </summary>
		/// <param name="type">The type of the object.</param>
		public LogTalker( in Type type ) {
			RepName = type.FullName; // Get's the full name of the type.
			manager = LogManager.Singleton; // Get's the log manager.
		}

		/// <summary>
		/// Writes out anything passed to it to the log manager.
		/// Please note that if it is an exception to use <see cref="Message(Exception, MessageType)"/> or <see cref="Message(string, Exception, MessageType)"/>.
		/// </summary>
		/// <param name="message">The message to log out.</param>
		/// <param name="type">What type of log it is.</param>
		public void Message( string message, MessageType type ) => manager.EnqueueLog( message, RepName, type: type );

		/// <summary>
		/// Writes out anything that is not a verbose message.
		/// If you want to write out a verbose message use either <see cref="Message(string, MessageType)"/> or <see cref="Message(string, Exception, MessageType)"/>.
		/// </summary>
		/// <param name="e">The exception to log out.</param>
		/// <param name="type">The type of log.</param>
		public void Message( Exception e, MessageType type ) {
			// Clearly checks to see if the message is of type verbose, then returns out of method.
			// Since this method is only meant to handle errors and exceptions.
			if ( type == MessageType.Verbose ) {
				return;
			}

			manager.EnqueueLog( null, RepName, e, type ); // Enqueues the log.
		}

		/// <summary>
		/// A general method for writing to the logs.
		/// Please use <see cref="Message(string, MessageType)"/> for verbose or errors, and <see cref="Message(Exception, MessageType)"/> for erros and exceptions.
		/// This method is great for including a custom message with an error.
		/// </summary>
		/// <param name="customMessage">The message that is either the only message, or a custom, seconday message to an error.</param>
		/// <param name="e">The exception to include.</param>
		/// <param name="type">The type of log.</param>
		public void Message( string customMessage, Exception e = null, MessageType type = MessageType.Verbose ) => manager.EnqueueLog( customMessage, RepName, e, type );
	}
}