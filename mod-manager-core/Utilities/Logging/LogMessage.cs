﻿using System;
using System.Text;

/* I've recreated LogManager for this program to be better.
 * Since LogManager is quite weird in how it is coded, this should help the program.
 * LogMessages is a struct that is meant to represent a message in the log.
 * These messages are not meant to stay around, hence why they are structs.
 */

namespace ModManagerCore.Utilities.Logging {

	/// <summary>
	/// Represents what type of message it is.
	/// </summary>
	public enum MessageType {
		/// <summary>
		/// Represents that the message should be written to the exception log file.
		/// </summary>
		Exception = 2,
		/// <summary>
		/// Represents that the message should be written to the error log file.
		/// </summary>
		Error = 1,
		/// <summary>
		/// Represents that the message should be written to the verbose log file.
		/// </summary>
		Verbose = 0
	}

	/// <summary>
	/// Represents a log in one of the 3 log files.
	/// </summary>
	public struct LogMessage {

		/// <summary>
		/// A string representing the standard log output.
		/// </summary>
		public string StandardOutput {
			get;
			private set;
		}

		/// <summary>
		/// The message or custom message that is placed with the log.
		/// </summary>
		public string Message {
			get;
		}

		/// <summary>
		/// Where the log originated from.
		/// </summary>
		public string From {
			get;
		}

		/// <summary>
		/// When the log occured.
		/// </summary>
		public DateTimeOffset Occured {
			get;
		}

		/// <summary>
		/// The optional exception that came with the log.
		/// </summary>
		public Exception Exception {
			get;
		}

		/// <summary>
		/// What type of log it is.
		/// </summary>
		public MessageType Type {
			get;
		}

		/// <summary>
		/// Creates a log information struct using the information it provided.
		/// </summary>
		/// <param name="message">The message of the log. Passing <see langword="null"/> is recommended if log type is exception.</param>
		/// <param name="from">Where the log is from.</param>
		/// <param name="e">The exception that comes with the log.</param>
		/// <param name="type">The type of the log.</param>
		public LogMessage( string message, string from, Exception e = null, MessageType type = MessageType.Verbose ) : this( message, from, DateTimeOffset.Now, e, type ) {
			// This method is left empty since it just provides the information the other constructor needs.
		}

		/// <summary>
		/// Creates a log information struct using the information it provided.
		/// </summary>
		/// <param name="message">The message of the log. Passing <see langword="null"/> is recommended if log type is exception.</param>
		/// <param name="from">Where the log is from.</param>
		/// <param name="occured">When did this log happen.</param>
		/// <param name="e">The exception that comes with the log.</param>
		/// <param name="type">The type of the log.</param>
		public LogMessage( string message, string from, DateTimeOffset occured, Exception e = null, MessageType type = MessageType.Verbose ) {
			Message = message;
			From = from;
			Exception = e;
			Type = type;
			StandardOutput = null;
			Occured = occured;

			CreateStandardOutput();
		}

		// This method exists to help break up the constructor.
		// This method is only called once and it stores what it did into the StandardOutput property as a means to cache it.
		private void CreateStandardOutput() {
			// Creates a new string builder for making the standard output.
			StringBuilder builder = new StringBuilder( From )
				.Append( " @ " )
				.Append( Occured.ToString( "u" ) )
				.Append( ": " );

			// Switches on which type to know what to put in the output.
			switch ( Type ) {
				default: // This is if the type is unknown.
				case MessageType.Verbose: // This is if type is verbose.
					builder.Append( Message ); // Simply adds the message since their is nothing else to add.
					break;
				case MessageType.Error: // This is if the type is an error.
					if ( !string.IsNullOrWhiteSpace( Message ) ) { // Checks if message is not empty, null, or contains whitespace.
						builder.Append( "Custom Message: " ).AppendLine( Message ); // Appends the message.
					}

					if ( Exception != null ) { // Checks to see if exception is not null.
						builder.Append( "Message: " ).AppendLine( Exception.Message ) // Appends the exception message.
							.Append( "Error Type: " ).AppendLine( Exception.GetType().FullName ) // Appends the exception type.
							.Append( "Stacktrace: " ).AppendLine( Exception.StackTrace ); // Appends the stacktrace.
					}
					break;
				case MessageType.Exception: // This is if the type is an exception.
					builder.Append( "Message: " ).AppendLine( Exception.Message ) // Appends the exception message.
							.Append( "Exception Type: " ).AppendLine( Exception.GetType().FullName ) // Appends the exception type.
							.Append( "Stacktrace: " ).AppendLine( Exception.StackTrace ); // Appends the stacktrace.
					break;
			}

			StandardOutput = builder.ToString().Trim(); // Builds it, trims it, then assigns it to the standard log output property.
		}
	}
}
