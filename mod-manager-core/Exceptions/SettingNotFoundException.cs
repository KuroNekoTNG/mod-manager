﻿using System;

namespace ModManagerCore.Exceptions {

	/// <summary>
	/// This exception is thrown when there is no settings file or settings in general for a plugin.
	/// </summary>
	public class SettingNotFoundException : Exception {

		/// <summary>
		/// The name of the plugin that requested the settings.
		/// </summary>
		public string PluginName {
			get;
		}

		/// <summary>
		/// Basic constructor for this exception that only takes the plugin's name.
		/// <seealso cref="SettingNotFoundException(string, string)"/>
		/// <seealso cref="SettingNotFoundException(string, string, Exception)"/>
		/// </summary>
		/// <param name="pluginName">The name of the plugin.</param>
		public SettingNotFoundException( string pluginName ) : this( pluginName, "Unable to find the setting file for a plugin." ) {

		}

		/// <summary>
		/// This constructor takes 2 strings, one for the name of the plugin and the other a custom message.
		/// <seealso cref="SettingNotFoundException(string)"/>
		/// <seealso cref="SettingNotFoundException(string, string, Exception)"/>
		/// </summary>
		/// <param name="pluginName">The name of the plugin.</param>
		/// <param name="message">The custom message for the exception.</param>
		public SettingNotFoundException( string pluginName, string message ) : base( message ) {
			PluginName = pluginName;
		}

		/// <summary>
		/// This constructor takes 2 strings, one for the name of the plugin and one for the custom message.
		/// It also takes an exception for when an inner exception causes it.
		/// </summary>
		/// <param name="pluginName">The name of the plugin.</param>
		/// <param name="message">The custom message of the exception.</param>
		/// <param name="innerException">An exception that resulted in this one.</param>
		public SettingNotFoundException( string pluginName, string message, Exception innerException ) : base( message, innerException ) {
			PluginName = pluginName;
		}
	}
}
