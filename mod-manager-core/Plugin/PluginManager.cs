﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

using ModManagerCore.CoreAttributes;
using ModManagerCore.Utilities.Logging;

namespace ModManagerCore.Plugin {
	/// <summary>
	/// PluginLoader loads plugins from plugin folders into the AppDomain.
	/// </summary>
	public class PluginManager {

		#region Static

		/// <summary>
		/// Used to retrive the PluginLoader singleton.
		/// </summary>
		public static PluginManager Singleton {
			get;
		}

		/// <summary>
		/// Holds a reference to the plugin folder for the program.
		/// </summary>
		public static string PluginFolder {
			get;
		}

		static PluginManager() {
			// Since both Linux and Windows share a great place to put plugins, it is checked to see if it is either on Linux or Windows.
			// If it is on macOS, it just uses the Application Support folder.
			// If it is on an unknown system, it just makes a folder next to it.
			if ( RuntimeInformation.IsOSPlatform( OSPlatform.Windows ) || RuntimeInformation.IsOSPlatform( OSPlatform.Linux ) ) {
				PluginFolder = Path.Combine(
					Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ),
					"Mod Manager",
					"Plugins" );
			} else if ( RuntimeInformation.IsOSPlatform( OSPlatform.OSX ) ) {
				PluginFolder = Path.Combine(
					Environment.GetFolderPath( Environment.SpecialFolder.UserProfile ),
					"Library",
					"Application Support",
					"Mod Manager",
					"Plugins" );
			} else {
				PluginFolder = "Plugins";
			}

			if ( !Directory.Exists( PluginFolder ) ) {
				Directory.CreateDirectory( PluginFolder );
			}

			Singleton = new PluginManager(); // Creates a new version of the singleton.
		}

		#endregion

		/// <summary>
		/// Grabs a plugin from the list of loaded plugins.
		/// </summary>
		/// <param name="index">The index of the plugin.</param>
		/// <returns></returns>
		public Plugin this[int index] => plugins[index];

		/// <summary>
		/// The number of plugins loaded.
		/// </summary>
		public int NumberOfPlugins => plugins.Count;

		private LogTalker log;
		private List<Plugin> plugins;

		/// <summary>
		/// Private constructor of the singleton.
		/// </summary>
		private PluginManager() {
			log = new LogTalker( this );
			plugins = new List<Plugin>();
		}

		/// <summary>
		/// Allows for enumerating through the plugins that have been loaded.
		/// </summary>
		/// <returns></returns>
		public IEnumerator<Plugin> GetPlugins() => plugins.GetEnumerator();

		/// <summary>
		/// Used for installing a plugin into the program.
		/// </summary>
		/// <param name="progress">Some way for the progress to be reported back.</param>
		/// <param name="pluginPath">The path to the .plg file.</param>
		public void InstallPlugin( IProgress<(string status, int progress)> progress, string pluginPath ) {
			string pluginTmpDir;

			progress.Report( ("Decompressing Plugin", 1) );

			pluginTmpDir = DecompressArchive( pluginPath );

			progress.Report( ("Looking for, and verifying against, checksum files.", 2) );

			CheckIntegrety( pluginTmpDir );

			progress.Report( ("Installing Plugin", 3) );

			MovePluginToInstall( ref pluginTmpDir );

			progress.Report( ("Done installing Plugin", 4) );
		}

		private void MovePluginToInstall( ref string pluginDir ) {
			pluginDir = pluginDir.Remove( pluginDir.LastIndexOf( "ModManager" ), 11 );

			Directory.Move( pluginDir, PluginFolder );
		}

		private bool CheckIntegrety( string pluginTmpDir ) {
			string shaFile = Path.Combine( pluginTmpDir, "Checksum", "plugin.sha" );
			string md5File = Path.Combine( pluginTmpDir, "Checksum", "plugin.md5" );

			if ( File.Exists( shaFile ) && !VerifyIntegrety( pluginTmpDir, shaFile, SHA256.Create() ) ) {
				return false;
			}

			if ( File.Exists( md5File ) && !VerifyIntegrety( pluginTmpDir, md5File, MD5.Create() ) ) {
				return false;
			}

			return true;
		}

		private bool VerifyIntegrety( string plgTmpDir, string checksumFile, HashAlgorithm hash ) {
			string oldWorkingDir = Environment.CurrentDirectory;
			bool status = true;

			// Wants to use the temp dir so relative paths can be used.
			Environment.CurrentDirectory = plgTmpDir;

			try {
				VerifyIntegrety( checksumFile, hash );
			} catch ( Exception e ) {
				status = false;

				log.Message( e, MessageType.Exception );
			}

			// Does not want to stay in temp folder.
			Environment.CurrentDirectory = oldWorkingDir;

			// Returns if checksum verification came back clean.
			return status;
		}

		private bool VerifyIntegrety( string checksumFile, HashAlgorithm hash ) {
			// Using statements to avoid having to ensure the streams are disposed of properly.
			using ( FileStream checksumStream = new FileStream( checksumFile, FileMode.Open, FileAccess.Read, FileShare.Read ) ) {
				using ( StreamReader checksumReader = new StreamReader( checksumStream ) ) {
					// Wants to verify against all of the lines.
					while ( !checksumReader.EndOfStream ) {
						byte[] genChecksum;
						string line = checksumReader.ReadLine();

						if ( string.IsNullOrWhiteSpace( line ) ) {
							continue;
						}

						// Less lines of code when inlining.
						if ( !TryGetLine( line, hash, out string filePath, out byte[] fileChecksum ) ) {
							throw new Exception( "Unable to get data." );
						}

						using ( FileStream file = new FileStream( filePath, FileMode.Open, FileAccess.Read, FileShare.Read ) ) {
							genChecksum = hash.ComputeHash( file );
						}

						// Checks to see if they are the same.
						for ( int i = 0; i < genChecksum.Length || i < fileChecksum.Length; ++i ) {
							if ( genChecksum[i] != fileChecksum[i] ) {
								return false;
							}
						}
					}
				}
			}

			return true;
		}

		private bool TryGetLine( in string line, in HashAlgorithm hash, out string filePath, out byte[] checksum ) {
			string[] split = line.Split( ' ' );
			string checksumHex = null;

			filePath = split[0];
			checksum = null;

			// Starts at 1 since the file path is already gotten, and is looking for the checksum.
			for ( int i = 1; i < split.Length; ++i ) {
				// Looking to see if it is the checksum.
				if ( !string.IsNullOrWhiteSpace( split[i] ) ) {
					checksumHex = split[i];
					break;
				}
			}

			// If the checksum is null, then there is no checksum. No checksum means it cannot continue.
			if ( checksumHex is null ) {
				return false;
			}

			checksum = new byte[checksumHex.Length / 2];

			for ( int i = 0; i < checksumHex.Length; i += 2 ) {
				checksum[i / 2] = Convert.ToByte( checksumHex.Substring( i, 2 ) );
			}

			return true;
		}

		private string DecompressArchive( string pluginPath ) {
			string tmpDir = Path.Combine(
				Path.GetTempPath(),
				$"ModManager-{pluginPath.Substring( pluginPath.LastIndexOf( Path.PathSeparator ) + 1 )}" );
			ZipArchive zip = new ZipArchive(
				new FileStream( pluginPath, FileMode.Open, FileAccess.Read, FileShare.Read ),
				ZipArchiveMode.Read );

			if ( !Directory.Exists( tmpDir ) ) {
				Directory.CreateDirectory( tmpDir );
			} else {
				RemoveSubDirs( tmpDir );
			}

			zip.ExtractToDirectory( tmpDir );

			return tmpDir;
		}

		private void RemoveSubDirs( string tmpDir ) {
			foreach ( FileSystemInfo fsEntries in new DirectoryInfo( tmpDir ).EnumerateFileSystemInfos() ) {
				fsEntries.Delete();
			}
		}

		#region Plugin Loading Methods
		/// <summary>
		/// Used to initialize the loading of plugins.
		/// </summary>
		public void LoadPlugins() {
			DirectoryInfo[] pluginFolders;

			log.Message( "Starting plugin loading." ); // Logs that it has started loading plugins.

			pluginFolders = GetPluginFolders(); // Grabs the plugin folders.

			foreach ( DirectoryInfo pluginDir in pluginFolders ) { // Loops through all the plugin folders.
				Plugin plugin = LoadPlugin( pluginDir ); // Loads the plugin.

				if ( plugin is null ) { // Checks if plugin is null.
					log.Message( // Writes out that it failed to load the plugin.
						string.Format( "There was an error loading a plugin from '{0}'. Please make sure that the Assembly file exists.", pluginDir.FullName ), // Doing this 
						type: MessageType.Error );

					continue; // Moves to the next directory.
				}

				plugins.Add( plugin ); // Adds the plugin into the list.
			}
		}

		private Plugin LoadPlugin( DirectoryInfo pluginDir ) {
			Assembly[] asms;
			DirectoryInfo pluginAsmFolder = null;
			Plugin plugin = null;

			foreach ( DirectoryInfo dir in pluginDir.EnumerateDirectories( "Assembly", SearchOption.TopDirectoryOnly ) ) { // Attempts to grab the assembly directory.
				if ( dir.Name == "Assembly" ) { // Checks if the name of the directory is, indeed Assembly.
					pluginAsmFolder = dir; // Assigns the dir to the pluginAsmFolder var.
					break; // Breaks out of the loop.
				}
			}

			if ( pluginAsmFolder is null ) { // Checks if the var is null.
				return null; // Returns null.
			}

			asms = LoadAssemblies( pluginAsmFolder ); // Loads the assemblies.

			RunStaticOnLoad( asms ); // Ensures that the StaticConstructorOnLoad attribute is ran.

			foreach ( Assembly asm in asms ) { // Loops through all the assemblies.
				Type pluginType = null;

				try {
					foreach ( Type type in asm.GetTypes() ) {
						if ( type.BaseType == typeof( Plugin ) ) {
							pluginType = type;
							break;
						}
					}

					if ( pluginType is null ) {
						throw new TypeLoadException( $"Unable to find the `Plugin` type in assembly `{asm.FullName}`." );
					}

					plugin = Activator.CreateInstance( pluginType, new PluginPack( pluginDir.FullName ) ) as Plugin;
				} catch ( Exception e ) {
					log.Message( "An exception has occured while trying to get plugin type.", e, MessageType.Exception ); // Writes out that an exception has occured.
					continue; // Moves on to the next assembly.
				}
			}

			return plugin; // Returns the plugin.
		}

		private Assembly[] LoadAssemblies( DirectoryInfo pluginAsmFolder ) {
			List<Assembly> assemblies = new List<Assembly>(); // Creates a new list of assemblies.

			foreach ( FileInfo file in pluginAsmFolder.EnumerateFiles( "*.dll", SearchOption.TopDirectoryOnly ) ) { // Loops through all the Dynamic Link Libraries in the assembly folder.
				Assembly asm = Assembly.LoadFile( file.FullName ); // Loads the assembly file.

				assemblies.Add( asm ); // Adds the assembly to the list.
			}

			return assemblies.ToArray(); // Returns an array from the list.
		}

		private DirectoryInfo[] GetPluginFolders() => new DirectoryInfo( PluginFolder ).GetDirectories( "*", SearchOption.TopDirectoryOnly ); // Returns all the plugin folders.

		/// <summary>
		/// This method is used to load all methods marked with the proper attribute.
		/// </summary>
		/// <param name="assemblies">An <see cref="IEnumerable{T}"/> of assemblies that will be loaded.</param>
		private void RunStaticOnLoad( in IEnumerable<Assembly> assemblies ) {
			foreach ( Assembly asm in assemblies ) {
				MethodInfo[] methods = asm.GetTypes() // Grabs all the types from the assembly.
					.SelectMany( t => t.GetMethods( BindingFlags.Static ) ) // Grabs all the methods from said types that are also static.
					.Where( m => m.IsConstructor && m.GetCustomAttributes( typeof( StaticConstructorOnLoad ), false ).Length > 0 ) // Filters out all the methods where method does not have StaticConstructorOnLoad.
					.ToArray(); // Creates an array.

				foreach ( MethodInfo method in methods ) { // Loops through all the methods in the array.
					method.Invoke( null, null ); // Invokes the method.
				}
			}
		}
		#endregion
	}

	/// <summary>
	/// PluginPack is a struct that delivers information to the plugin for it's own use.
	/// </summary>
	public struct PluginPack {

		/// <summary>
		/// The folder the plugin is in.
		/// </summary>
		public string PluginFolder {
			get;
		}

		/// <summary>
		/// Used to make a PluginPack for use by the plugin object.
		/// </summary>
		/// <param name="pluginFolder">The folder the plugin resides in.</param>
		public PluginPack( string pluginFolder ) {
			PluginFolder = pluginFolder;
		}

	}
}