﻿using System;

namespace ModManagerCore.Plugin {
	/// <summary>
	/// This class represents information about a mod, including where to download it, the homepage of the mod, and a URL to the image of the mod.
	/// </summary>
	public class ModInformation {

		/// <summary>
		/// The name of the mod.
		/// </summary>
		public string Name {
			get;
		}

		/// <summary>
		/// The description of the mod.
		/// </summary>
		public string Description {
			get;
		}

		/// <summary>
		/// The version of the mod.
		/// </summary>
		public string Version {
			get;
		}

		/// <summary>
		/// The URL to download the mod.
		/// </summary>
		public Uri DownloadUrl {
			get;
		}

		/// <summary>
		/// The site the mod is hosted at.
		/// </summary>
		public Uri ModSite {
			get;
		}

		/// <summary>
		/// The URL to the image of the mod.
		/// </summary>
		public Uri ImageUrl {
			get;
		}

        public bool Enabled
        {
            get;
            set;
        }

		/// <summary>
		/// The constructor of the class.
		/// </summary>
		/// <param name="name">The name of the mod.</param>
		/// <param name="desc">The description of the mod.</param>
		/// <param name="version">The version of the mod.</param>
		/// <param name="downloadUrl">The direct link to the download of the mod.</param>
		/// <param name="modSite">The site where the mod is hosted at.</param>
		/// <param name="imageUrl">The URL to the image of the mod.</param>
		public ModInformation( string name, string desc, string version, Uri downloadUrl, Uri modSite, Uri imageUrl ) {
            Enabled = false;
			Name = name;
			Description = desc;
			Version = version;
			DownloadUrl = downloadUrl;
			ModSite = modSite;
			ImageUrl = imageUrl;
		}
	}
}