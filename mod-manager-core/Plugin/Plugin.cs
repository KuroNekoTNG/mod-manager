﻿using System;
using System.Collections.Generic;

using ModManagerCore.Utilities.Logging;

namespace ModManagerCore.Plugin {

	/// <summary>
	/// This <see langword="abstract"/> class represents a plugin and the basic communication between the front end and the backend of the program.
	/// </summary>
	public abstract class Plugin {

		/// <summary>
		/// Grabs a plugin at a index.
		/// </summary>
		/// <param name="index">The position of the mod information.</param>
		/// <returns>A <see cref="ModInformation"/> object.</returns>
		public ModInformation this[int index] => mods[index];

		/// <summary>
		/// The amount of mods found by the plugin.
		/// </summary>
		public int ModCount => mods.Count;

        /// <summary>
        /// Represents the plugin icon.
        /// This is in Uri format.
        /// </summary>
        public virtual Uri PluginIcon
        {
            get => null; // Returns a null Uri
        }

		/// <summary>
		/// Represents the name of the plugin.
		/// It is recommended to override this method with the nameof language keyword followed by the class name or a regular string.
		/// </summary>
		public virtual string PluginName => "Unnamed Plugin"; // Returns the Unnamed Plugin string.

		/// <summary>
		/// Holds information pertaining to information about the mod's current location and status.
		/// </summary>
		protected PluginPack pluginPack;

		/// <summary>
		/// This <see cref="List{T}"/> of <see cref="ModInformation"/> holds all the mods that are found.
		/// </summary>
		protected List<ModInformation> mods;

		/// <summary>
		/// Used to write out to the log files.
		/// </summary>
		protected LogTalker log;

		/// <summary>
		/// Default constructor for any plugin.
		/// When making a plugin, ensure that this is called.
		/// </summary>
		/// <param name="pluginPack">Holds data relating to the state of the plugin.</param>
		public Plugin( PluginPack pluginPack ) {
			this.pluginPack = pluginPack; // Assigns the plugin pack to the protected variable.

			mods = new List<ModInformation>(); // Creates a new List of ModInformation.
			log = new LogTalker( this ); // Creates a new log talker for the object to use.
		}

		/// <summary>
		/// Returns an enumerator to enumerate over a list of mod informations.
		/// </summary>
		/// <returns>An <see cref="IEnumerator{T}"/> of <see cref="ModInformation"/> to loop through.</returns>
		public IEnumerator<ModInformation> GetModInformation() => mods.GetEnumerator();

		/// <summary>
		/// This method is called to update the list of mods.
		/// </summary>
		public abstract void UpdateModList();

		/// <summary>
		/// Invoked to install the list of mods passed to via an enumerator.
		/// </summary>
		/// <param name="mods">An enumerator of ints.</param>
		/// <returns>True on success of fully installing mods.</returns>
		public abstract bool InstallMods( IEnumerable<int> mods );

		/// <summary>
		/// Invoked to install drag and drop mods or mods passed by a path.
		/// </summary>
		/// <param name="mods">An enumerable list of mods.</param>
		/// <returns>True on success of fully installing mods.</returns>
		public abstract bool InstallMods( IEnumerable<string> mods );

	}
}