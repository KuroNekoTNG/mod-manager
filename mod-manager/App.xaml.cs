﻿using System;
#if DEBUG
using System.Diagnostics;
#endif
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows;

using ModManager.UI;

using ModManagerCore.Plugin;
using ModManagerCore.Utilities;
using ModManagerCore.Utilities.Logging;

// Look at the Assets/Text/help.txt file for a list of arguments that the program take.

namespace ModManager {
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application {

		/// <summary>
		/// The string used to display the exception message.
		/// </summary>
		public static readonly string MAIN_WINDOW_EXCEPTION_STR;

		/// <summary>
		/// This string is used to display a list of commands that can be passed to it.
		/// </summary>
		public static readonly string CONSOLE_HELP_STR;

		// This is a static constructor.
		static App() {
			// Grabs the warning message from the assembly.
			MAIN_WINDOW_EXCEPTION_STR = EmbededResourceLoader.LoadTextFromAssembly( typeof( App ), "ModManager.Assets.Text.MainWindowCrash.txt" );
			// Grabs the help string from the assembly.
			CONSOLE_HELP_STR = EmbededResourceLoader.LoadTextFromAssembly( typeof( App ), "ModManager.Assets.Text.help.txt" );
		}

		private bool skipMainWindow = false;

		private PluginManager pluginManager;
		private LogTalker log;

		public App() : base() {
			log = new LogTalker( this ); // Creates a new logger.
			pluginManager = PluginManager.Singleton; // Grabs the plugin manager.
			Exit += ( _, __ ) => LogManager.Singleton.InitiateClosing(); // Ensures that the log manager closes when the UI is done.
		}

		internal void RestartProgram() {

		}

		internal void RestartProgram( bool install = true, params string[] plugins ) {
			ProcessStartInfo startInfo = GetStartInfo();

			startInfo.Arguments = GenerateCommandLineArgs( install, plugins );
		}

		private string GenerateCommandLineArgs( bool install, string[] plugins ) {
			StringBuilder builder = new StringBuilder();

			foreach ( string plugin in plugins ) {
				if ( install ) {
					builder.Append( "--install-plugin " );
				} else {
					builder.Append( "--remove-plugin " );
				}

				builder.Append( plugin ).Append( ' ' );
			}

			return builder.ToString().Trim();
		}

		private ProcessStartInfo GetStartInfo() {
			string exePath = Assembly.GetEntryAssembly().Location;

			return new ProcessStartInfo( exePath );
		}

		// This is the start of the program.
		private void AppStartup( object sender, StartupEventArgs args ) {
#if DEBUG
			Debugger.Launch(); // Attaches to the debugger.
#endif
			ProcessArgs( args.Args ); // Calls this method to process args.

			if ( !skipMainWindow ) { // Checks if it wants to skip the loading of the main window.
				pluginManager.LoadPlugins(); // Calls this to start the plugin loading process.

				MainWindow = new MainWindow(); // Creates a new MainWindow object for displaying.

				MainWindow.Closed += ( _, __ ) => Shutdown(); // Forwards the call to the shutdown method.

				try {
					MainWindow.Show(); // Show's the MainWindow. 
				} catch ( Exception e ) { // Catches all exception.
					log.Message( "An exception occured while trying to show the main window.", e, MessageType.Exception ); // Writes out the exception to the log.

					ReportCriticlException( e ); // Calls method to report to user.
				}
			}
		}

		// This is to report when the main window fails to load.
		private void ReportCriticlException( Exception e ) {
			MessageBoxResult result = MessageBox.Show( // Starts the showing of the message box.
				string.Format( MAIN_WINDOW_EXCEPTION_STR, e.GetType().FullName, e.StackTrace ), // Formats the string to add the exception type and stacktrace.
				"Error Showing Window", // Title of message box.
				MessageBoxButton.YesNo, // Options for message box.
				MessageBoxImage.Exclamation, // Icon for message box.
				MessageBoxResult.Yes // Default option for message box.
			);

			switch ( result ) { // Switches on the result.
				case MessageBoxResult.Yes: // If the result is yes.
					RestartProgram(); // Calls this method to restart the program/
					break; // Breaks out of the switch.
				case MessageBoxResult.No: // If the result is no.
				default: // Any other enum.
					return; // Breaks out of the method.
			}
		}

		// Used to process the args.
		private void ProcessArgs( string[] args ) {
			for ( int i = 0; i < args.Length; ++i ) { // Loops through the args.
				switch ( args[i] ) { // Switches on the string.
					case "--install-plugin": // Checks if it is the install plugin arg.
						try {
							PluginOperation operationWindow = new PluginOperation( args[++i] );

							operationWindow.ShowDialog();
						} catch ( Exception e ) {
							log.Message( e, MessageType.Error );
						}
						break; // Breaks out of the switch.
					case "--remove-plugin": // Checks if it is the remove plugin arg.
						try {
							PluginOperation operationWindow = new PluginOperation( args[++i], true );

							operationWindow.ShowDialog();
						} catch ( Exception e ) {
							log.Message( e, MessageType.Error );
						}
						break; // Breaks out of the switch.
					case "--help": // If it is the help arg.
					case "-h": // If it is the short hand help arg.
					default: // Unknown args.
						PrintHelp();
						break; // Breaks out of the switch.
				}
			}
		}

		private void PrintHelp() {
			string helpFilePath = Path.Combine( // Creates a new path for the help file.
				Environment.GetFolderPath( Environment.SpecialFolder.DesktopDirectory ), // Grabs the desktop dir.
				"ModManager Help.txt" ); // Set's the name of the file.

			using ( FileStream helpFileStream = new FileStream(
				helpFilePath, // Set's the path to the path created above.
				FileMode.Create, // Tells Windows to create or overwrite the file.
				FileAccess.Write, // Grants it write access.
				FileShare.Read, // Allows other programs to read from said file.
				1, // This is the buffer size that will not be used.
				FileOptions.WriteThrough ) ) { // Will write through to the disk.
				byte[] stringBytes = Encoding.Default.GetBytes( CONSOLE_HELP_STR ); // Gets the bytes that represent the string.

				helpFileStream.Write( stringBytes, 0, stringBytes.Length ); // Writes the bytes to the file.
				helpFileStream.Flush( true ); // Flushes changes to disk.
			}

			MessageBox.Show(
				$"The help file has been created at '{helpFilePath}'. If you need it again, please retype the command again.", // The message of the MessageBox.
				"Help File Created.", // The title of the MessageBox.
				MessageBoxButton.OK, // What buttons appear.
				MessageBoxImage.Information ); // What symbol appears on the box.

			Shutdown(); // Shutdown the application.
		}
	}
}
