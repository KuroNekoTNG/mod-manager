﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using ModManagerCore.Plugin;

namespace ModManager.UI {
	/// <summary>
	/// Interaction logic for PluginOperation.xaml
	/// </summary>
	public partial class PluginOperation : Window {

		private Progress<(string, int)> progressDispatch;
		private PluginManager pluginManager;
		private Task task;
		private Thread waitThread;

		public PluginOperation( string pluginPath, bool uninstallPlugin = false ) {
			progressDispatch = new Progress<(string, int)>( ReportProgress ); // Creates a new Progress class.
			pluginManager = PluginManager.Singleton; // Grabs the plugin manager.
			waitThread = new Thread( () => { // Creates a new thread.
				while ( !task.IsCompleted ) { // Loops until the task is completed.
					Thread.Sleep( 1000 ); // Sleeps the thread for a second.
				}

				Dispatcher.Invoke( () => Close() ); // Invokes the Close() method on the UI thread.
			} ) {
				Name = "Plugin Operation Wait Thread", // Set's the name of the thread.
				Priority = ThreadPriority.BelowNormal // Set's the priority of the thread.
			};

			InitializeComponent(); // Initializes all the components.

			Title = uninstallPlugin ? "Uninstalling Plugin" : "Installing Plugin"; // Set's reather the title should be installing plugin or uninstalling plugin.

			if ( uninstallPlugin ) { // Checks if the operation is to uninstall the plugin.
									 // TODO: Make and call the uninstall method of PluginManager.
			} else {
				pluginManager.InstallPlugin( progressDispatch, pluginPath ); // Makes an async call to install the mod.
			}

			waitThread.Start(); // Starts the thread.
		}

		private void ReportProgress( (string status, int progress) tuple ) => ReportProgress( tuple.status, tuple.progress );

		private void ReportProgress( string stat, int prog ) {
			status.Content = stat; // Set's the string status to the tuple status.

			if ( prog < 0 ) { // Checks if progress is less than 0.
				progress.IsIndeterminate = true; // Set's indeterminate to true.
			} else {
				progress.IsIndeterminate = false; // Set's indeterminate to false.
				progress.Value = prog; // Set's the progress bar to be that far.
			}
		}
	}
}
