using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Controls;

using ModManagerCore.Plugin;
using ModManagerCore.Utilities.Settings;

namespace ModManager {
	/// <summary>
	/// Interaction logic for SettingsControl.xaml
	/// </summary>
	public partial class SettingsControl : UserControl {
		public SettingsControl() {
			InitializeComponent(); // Initializes the xaml
			InitPluginSettings(); // Initializes settings for any loaded plugins
		}

		/// <summary>
		/// Initializes the setting categories for all loaded plugins.
		/// </summary>
		private void InitPluginSettings() {
			// Gets the plugin manager and settings managers
			PluginManager pluginManager = PluginManager.Singleton;
			SettingsManager settingsManager = SettingsManager.Singleton;

			// Loop through the plugin manager and make an Expander for each plugin
			for ( int i = 0; i < pluginManager.NumberOfPlugins; ++i ) {
				Plugin plugin = pluginManager[i]; // Get the plugin at index i
				Expander category = new Expander // Create new expander for that plugin
				{
					Header = plugin.PluginName,
				};

				// Create a settings entry in the settings manager for plugin
				if ( settingsManager.TryCreateSetting( plugin.PluginName ) ) {
					// If success then we need to get all settings entries here.
					// TODO: Figure out how to populate the settings.
					for ( int j = 0; j < 1; ++j ) {
						// THIS IS OBVIOUSLY INCORRECT
						settingsManager.TryGetSetting( plugin.PluginName, "ConstructorHandling", out object test );
					}

					// This list will need to be populated with each setting entry. (Probably not a string)
					List<string> settingEntires = new List<string>();
					// Embed a stack panel into the expander to list out all settings
					category.Content = new StackPanel {
						// Stack panel populated with entries from the settingsEntries list
						DataContext = settingEntires
					};
				}

				SettingsPanel.Children.Add( category ); // Add plugin's category to the settings panel
			}
		}

		private void InstallPlugin_Btn( object sender, System.Windows.RoutedEventArgs e ) {
			if ( RuntimeInformation.IsOSPlatform( OSPlatform.Windows ) ) { // if windows 
				Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog {
					Multiselect = true,
					DefaultExt = ".dll", // Default type is .zip
					Filter = "Assembly Files|*.dll|All Files|*" // Filter for archives or all files
				}; // open file dialog
				Nullable<bool> result = openFileDlg.ShowDialog(); // Show the file browser
				if ( result == true ) // if there is a file
				{
					foreach ( string pluginFilePath in openFileDlg.FileNames ) { // for every plugin selected
																				 // Get the destination folder
						string destination = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ),
							"ModManager", "Plugin", Path.GetFileNameWithoutExtension( pluginFilePath ), "Assembly" );

						if ( !Directory.Exists( destination ) ) { // Makes sure the setting directory exists
							Directory.CreateDirectory( destination ); // Create the directory if doesn't exist
						}
						// Copy the dll to the assembly folder
						File.Copy( pluginFilePath, Path.Combine( destination, Path.GetFileName( pluginFilePath ) ), true );
					}
				}
			}
		}
	}
}
