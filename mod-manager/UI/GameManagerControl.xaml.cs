﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

using ModManagerCore.Plugin;

namespace ModManager.UI {
	/// <summary>
	/// GameManagerControl is the template for any plugin to manage a
	/// game's mods. 
	/// </summary>
	public partial class GameManagerControl : UserControl {
		private string gameExePath;
		private Plugin plugin;

		/// <summary>
		/// Constructor. Passed a plugin reference to create a control for that plugin.
		/// </summary>
		/// <param name="plugin"></param>
		public GameManagerControl( Plugin plugin ) {
			InitializeComponent();
			this.plugin = plugin;
			OnInitStart( plugin );
		}

		/// <summary>
		/// Initializes the control's data with the plugin's data.
		/// </summary>
		private void OnInitStart( Plugin plugin ) {
			// Load column categories into the DataGrid (mod list) for any custom columns
			// Populate the DataGrid 
			// Associate each DataGrid mod with a mod info panel

			// Create a list of mods
			List<ModInformation> mods = new List<ModInformation>();
			for ( int i = 0; i < plugin.ModCount; ++i ) {
				mods.Add( plugin[i] );
			}
			// bind mods list to modsPanel
			modsPanel.DataContext = mods;
		}

		/// <summary>
		/// Implements the functionality for the game run button.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void GameRunButton( object sender, RoutedEventArgs e ) {
			// plugin should have a game .exe associated with it. Button should run that .exe
			if ( gameExePath == null ) {
				MessageBox.Show( "No game .exe found. Change this in the plugin's settings." );
			} else {
				Process.Start( gameExePath );
			}
		}

		/// <summary>
		/// Implements the functionality for the install mod button.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void InstallModButton( object sender, RoutedEventArgs e ) {
			string[] modFilePath; // stores file path
			bool? result;

			Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog {
				Multiselect = true,
				DefaultExt = ".zip", // Default type is .zip
				Filter = "Archive Files|*.zip|All Files|*" // Filter for archives or all files
			}; // open file dialog

			result = openFileDlg.ShowDialog(); // Show the file browser

			if ( result == true ) // if there is a file
			{
				modFilePath = openFileDlg.FileNames; // get the file name
				plugin.InstallMods( modFilePath ); // install the mods
			}
		}

		/// <summary>
		/// Implements functionality for refresh mods button.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void RefreshModsButton( object sender, RoutedEventArgs e ) => plugin.UpdateModList();

		/// <summary>
		/// Implements functionality for dragging and dropping mod files into the program for installation
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void DragDropModInstall( object sender, DragEventArgs e ) {
			string[] files = ( string[] )e.Data.GetData( DataFormats.FileDrop );
			plugin.InstallMods( files );
		}

		private void ModsPanel_SelectedCellsChanged( object sender, SelectedCellsChangedEventArgs e ) {
			int index = modsPanel.SelectedCells.IndexOf( modsPanel.SelectedCells[0] );
			modInfoPanel.Content = new ModInfoControl( plugin[index] );

		}
	}
}
