﻿using System.Windows.Controls;
using System.Windows.Media.Imaging;

using ModManagerCore.Plugin;

namespace ModManager.UI {
	/// <summary>
	/// Interaction logic for ModInfoControl.xaml
	/// </summary>
	public partial class ModInfoControl : UserControl {
		public ModInfoControl( ModInformation mod ) {
			InitializeComponent();
			modName.Text = mod.Name;
			modDescr.Text = mod.Description;
            if (mod.ImageUrl != null)
            {
                modImage.Source = new BitmapImage(mod.ImageUrl);
            }
		}
	}
}
