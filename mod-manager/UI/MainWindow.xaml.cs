using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

using ModManagerCore.Plugin;

namespace ModManager.UI {
	/// <summary>
	/// The main window for the Mod Manager Program.
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		private List<TabItem> loadedTabs; // List that maintains currently loaded tabs (Plugins).
		private PluginManager pluginManager; // Instance of the PluginManager
		public MainWindow() {
			InitializeComponent();

			// Retriveves a instance of the plugin manager?
			// Not entirely sure on the singleton concept yet.
			pluginManager = PluginManager.Singleton;

			// Initialize the list of currently loaded plugins.
			loadedTabs = new List<TabItem>();
			loadSettingsPanel();
			LoadPlugins();

			// Use the loaded plugins list for the TabControl's tabs
			PluginTabs.ItemsSource = loadedTabs;
		}

		private void loadSettingsPanel() {
			TabItem settings = new TabItem {
				Header = "Settings",
				Content = new SettingsControl()
			};
			loadedTabs.Add( settings );
		}

		/// <summary>
		/// Loads the plugins utilizing the PluginManager.
		/// Iterates over the PluginManager, and calls CreateTab
		/// for each plugin.
		/// </summary>
		private void LoadPlugins() {
			// Loop over the PluginManager 
			for ( int i = 0; i < pluginManager.NumberOfPlugins; ++i ) {
				Plugin plugin = pluginManager[i]; // Get plugin at index
				TabItem tab = CreateTab( plugin ); // create a TabItem from the plugin
				loadedTabs.Add( tab ); // Add tab to list of current tabs
			}
		}
		/// <summary>
		/// Creates a TabItem for the passed plugin.
		/// </summary>
		/// <param name="plugin"></param>
		/// <returns>TabItem of passed plugin</returns>
		private TabItem CreateTab( Plugin plugin ) {
			//if plugin type not default then
			//  TabItem content = associated custom control
			TabItem tab = new TabItem {
				Header = plugin.PluginName,
				// Assume all tabs are GameManagerControls for now
				// TODO: get plugin type and corresponding control
				Content = new GameManagerControl( plugin )
			};
			return tab;
		}
	}
}
