# 0.0.1
*  Started work on User Interface.
*  Core as already been started.
*  General start.

# 0.3.3
*  Reworked Plugin System
    *  Previous plugin system was too complicated.
*  Added a way for static constructors to be loaded.
*  Improved UI.
*  Work on new Plugin System done.

# 0.5.5
*  Added more to the plugin system.
*  Fixed some issues with the UI.
*  Better seperated files.
*  General bug fixes.

# 0.8.7
*  Work on removing LogHelper.dll dependency.
    *  New internal log management system has been put into place for better consistancy.
    *  New log management system also has some fixes compared to LogHelper.
*  Did more seperation.
*  Cleaned code up.

# 0.9.2
*  Added some documentation.
*  Added a settings system.
    *  Settings does not keep settings loaded into memory.
*  Reworked main window.
*  General bug fixes.

# 0.9.5
*  LogHelper dependency now fully removed.
*  Fixed major issue preventing plugins from loading.
*  More bug fixes.
*  Changed behaviour of plugins when they first start.
*  UI functionality added.